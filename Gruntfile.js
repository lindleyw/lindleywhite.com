'use strict';
module.exports = function(grunt) {

  grunt.initConfig({
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: [
        'Gruntfile.js',
        'assets/js/main.js'
      ]
    },
    compass: {                  // Task
        dist: {                   // Target
          options: {              // Target options
            sassDir: 'assets/sass',
            cssDir: 'assets/styles'

          }
        }
    },
    uglify: {
      dist: {
        files: {
          'assets/js/scripts.min.js': [
            'assets/js/plugins.js',
            'assets/js/main.js'
          ]
        }
      }
    },
    watch: {
      sass: {
        files: [
          'assets/sass/*.scss',
          'assets/scss/*.scss'
        ],
        tasks: ['compass']
      },
      js: {
        files: [
          'assets/js/main.js',
          'Gruntfile.js'
        ],
        tasks: ['jshint', 'uglify']
      },
      livereload: {
        // Browser live reloading
        // https://github.com/gruntjs/grunt-contrib-watch#live-reloading
        options: {
          livereload: false
        },
        files: [
          'assets/styles/styles.css',
          'assets/js/scripts.min.js',
          'site/templates/*.php',
          '*.php'
        ]
      }
    },
    clean: {
      dist: [
        'assets/css/main.min.css',
        'assets/js/scripts.min.js'
      ]
    }
  });

  // Load tasks
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-compass');

  // Register tasks
  grunt.registerTask('default', [
    'clean',
    'compass',
    'uglify',
    //'version'
  ]);
  grunt.registerTask('dev', [
    'watch'
  ]);

};
