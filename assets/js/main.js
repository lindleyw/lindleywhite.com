var Ww = $(window).width();


var makeMovement ={
	init: function() {
		makeMovement.projectHover();
		makeMovement.loadProject();
		//makeMovement.exitBackTo();

	},
	loadProject: function() {
		$('.project-list li .image-wrapper').click(function(e){
			e.preventDefault();
			wispAway();

			var post_id = $(this).closest('a').attr('rel');

			initLoad(post_id);

		});

		$(window).ready(function(){
			checkPageIsProject();
		});

		$(window).on('scroll', function(){
			if (Ww > 768){
				$('.pagination')
				.stop()
				.animate({"top": ($(window).scrollTop() + 230)+ 'px'}, 800);
			}else {
				$('.pagination')
				.stop()
				.animate({"top": ($(window).scrollTop() + 80)+ 'px'}, 800);
			}


		});
		function onSuccess(data){
			$('#project-container').append(data).fadeIn();
			$('#project-container').addClass('animate fadeInRight');

			registerPagiClick();
			scrollTop();
		}

		function wispAway(){
			$('#projects').addClass('animate fadeOutLeftBig collapse');
		}

		function initLoad(post_id){
			$.get('/ajax/',{id:post_id})
			.done(function(data){
				onSuccess(data);
			});
		}

		function clearDiv(){
			$('#project-container').empty().removeClass('animate fadeInRight');
		}

		function registerPagiClick(){
			$('.paginate a').click(function(e){
				e.preventDefault();
				console.log($(this));
				var article_id = $(this).attr('rel');
				clearDiv();
				initLoad(article_id);
			});
		}

		function scrollTop(){
			$(window).scrollTop(0);
		}

		function checkPageIsProject(){
			if($(document.body).hasClass('projects')) {
				registerPagiClick();
			}
		}


	},
	projectHover: function() {
		$('.project-list li').hover(function(){
			if(Ww > 768){
				$(this).find('.wrap').toggleClass('animate fadeInUp');
			}
		});
	},
	exitBackTo: function() {

			$('#projects').removeClass('fadeOutLeftBig').addClass('fadeInLeftBig');

	}



};

var tumblrPosts = {

	getTumblePosts: function() {
		$.ajax({
			type: "GET",
			url: "https://api.tumblr.com/v2/blog/digitalspray.tumblr.com/posts",
			dataType: "jsonp",
			data: {
				api_key: "4NRZxyNuqVuy7exLRCEZbnuXREk3jJqIOB4yzXtEoy0sCxl4I7",
				jsonp: "tumblrPosts.getTumbleCallback()"
			}
		});
	},
	getTumbleCallback: function(data){
			console.log(data.response.posts);
	}
};


$(window).ready(function() {

	//Call Movement Object
	makeMovement.init();
	tumblrPosts.getTumblePosts();

});