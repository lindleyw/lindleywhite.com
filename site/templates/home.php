<?php snippet('header') ?>
<div id="main">

  <?php snippet('sidebar') ?>

<section id="content-holder">
   <section id="projects">
      <h1>
        <span></span>
      </h1>
      <div class="main-wrap">
        <div class="main-content">
          <h2>Lindley White</h2>
          <h3 class="tagline">
            <p>
              <?php echo $page->text() ?>
            </p>
          </h3>
        </div>
      </div>
      <h1>
        <span>Recent Freelance Projects</span>
      </h1>
      <ul class="project-list">
        <?php snippet('foreach-project') ?>
      </ul>
   </section>


   <section id="project-container"></section>
   <?php snippet('footer') ?>
</section>

</div> <!-- End of #main -->
