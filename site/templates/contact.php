<?php snippet('header') ?>

<div id="main">

  <?php snippet('sidebar') ?>

<section id="content-holder">

  <div id="content-wrapper" class="animate fadeInRight">
    <h1>Contact</h1>
    <section id="contact">

      <p>Feel free to send me an email at <a href="mailto:lindley@lindleywhite.com">lindley@lindleywhite.com</a>, otherwise you can follow me on the following services.</p>
      <table>
        <thead>
        <tr>
        <th></th>
        <th>Service</th>
        <th>Username</th>
        <th>URL</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td><img src="/assets/images/twitter.png" alt="Twitter"></td>
          <td>Twitter</td>
          <td>@lindleywhite</td>
          <td><a href="http://twitter.com/lindleywhite">twitter.com/lindleywhite</a></td>
        </tr>
        <tr>
          <td><img src="/assets/images/github.png" alt="Github"></td>
          <td>GitHub</td>
          <td>lindleywhite</td>
          <td><a href="http://github.com/lindleywhite">github.com/lindleywhite</a></td>
        </tr>
        <tr>
          <td><img src="/assets/images/spotify.png" alt="Spotify"></td>
          <td>Spotify</td>
          <td>Lindley White</td>
          <td><a href="https://play.spotify.com/user/LindleyWhite">play.spotify.com/user/LindleyWhite</a></td>
        </tr>
          </tbody>
        </table>
    </section>
  </div>

<?php snippet('footer') ?>
</section>




</div> <!-- End of #main -->