<?php
	$id = get('id');
	$article = $pages->find('projects/' . get('id'));

	// if no article could be found for that id…
	if(!$article){
		die('Invalid article ID');
	}
?>

<h2 class="project header"><span><?php echo $article->title(); ?></span></h2>
<div class="pagination wrapper">
<?php

      if($article->hasNextVisible()){

          $nextObj = $article->nextVisible();
          // var_dump($nextObj);


          $nextID = $nextObj->uid();
          // var_dump($nextID);
            ?>

          <div class="project-next paginate">
            <a href="#<?php echo $nextID; ?>" rel="<?php echo $nextID; ?>">
              <h4>Next Project:</h4> <h3><?php echo $nextObj->title(); ?></h3>
            </a>
          </div>
      <?php  }


      if($article->hasPrevVisible()){
        $prevObj = $article->prevVisible();
        $prevID = $prevObj->uid();
      ?>

        <div class="project-prev paginate">
          <a href="#<?php echo $prevID; ?>" rel="<?php echo $prevID; ?>">
            <h4>Previous Project:</h4> <h3><?php echo $prevObj->title(); ?></h3>
          </a>
        </div>

    <?php } ?>

</div>
<div class="project wrapper">


  <div class="content">

  <?php echo kirbytext($article->text()) ?>
  <?php if($article->link()): ?>
  <a class="live link" href="<?php echo $article->link() ?>" target="_blank"><?php echo $article->linktext() ?></a>
  <?php endif; ?>
  <div class="role devel"><span>Role:</span> <?php echo $article->role(); ?></div>
  <div class="credit"><?php echo $article->credit() ?></div>

  </div>

  <ul class="slides images">
  <?php foreach($article->images() as $image): ?>
  	<?php if($image->hasNext()){   $image = $image->next();  ?>

  		<li>
      <img src="<?php echo $image->url() ?>"  alt="<?php echo $image->name() ?>" />
    </li>

  	<?php } ?>

  <?php endforeach; ?>


  </ul>

</div>


