<?php snippet('header') ?>

<div id="main">

  <?php snippet('sidebar') ?>

<section id="content-holder">

<?php if($page->isHomePage()){ ?>

  <section id="projects">
    <h1>
      <span>Featured Projects</span>
    </h1>
    <ul class="project-list">
      <?php snippet('foreach-project') ?>
    </ul>
  </section>
  <section id="project-container"></section>

<?php } elseif ($page->parent->uid() == 'projects') {
  snippet('one-project');
} ?>

<?php snippet('footer') ?>
</section>




</div> <!-- End of #main -->
