<?php snippet('header') ?>


<div id="main">

  <?php snippet('sidebar') ?>

<section id="content-holder">
	<div id="content-wrapper" class="animate fadeInRight">

		<div id="about">
			<!-- <h1 class="about-title"><?php echo $page->header(); ?></h1> -->
			<div class="profile-image"></div>
			<?php echo kirbytext($page->text()); ?>

		</div>
	</div>
   <?php snippet('footer') ?>
</section>

</div> <!-- End of #main -->