<aside id="sidebar">
    <div class="branding"><a href="<?php echo $site->url() ?>"><img src="<?php echo u('assets/images/logo_lw.png') ?>" /></a></div>
    <nav class="menu">
		  <ul>
		    <?php foreach($pages->visible() AS $p): ?>
		    <li<?php echo ($p->isOpen()) ? ' class="active"' : '' ?>>
		    	<a href="<?php echo $p->url() ?>" rel="<?php echo $p->uid() ?>">
		    		<h1><?php echo excerpt($p->title()) ?></h1>
		    		<h2><?php echo  excerpt($p->sub_nav()) ?></h2>
		    	</a>
		    </li>
		    <?php endforeach ?>
		  </ul>
	</nav>

	<div class="social">
		<ul class="links">
			<li>
				<h2>Twitter</h2>
				<a href="http://twitter.com/lindleywhite" class="link">@lindleywhite.com</a>
			</li>
			<li>
				<h2>E-mail Me</h2>
				<a href="mailto:lindley@lindleywhite.com" class="link">lindley@lindleywhite.com</a>
			</li>
		</ul>

	</div>

	
</aside>