   <section id="project-container">
     <?php 
       
      
       $article = $page;

       if(!$article){
         die('Invalid article ID'); 
       }
     ?>

     <h2 class="project header"><span><?php echo $article->title(); ?></span></h2>
     <div class="pagination wrapper">
     <?php 

           if($article->hasNext()){

               $nextObj = $article->next();
               $nextLink = get_object_vars($nextObj);
               $nextID = $nextLink['_']['uid']; ?>

               <div class="project-next paginate">
                 <a href="#<?php echo $nextID; ?>" rel="<?php echo $nextID; ?>"> 
                   <h4>Next Project:</h4> <h3><?php echo $nextObj->title(); ?></h3>
                 </a>
               </div>
           <?php  }       


           if($article->hasPrev()){
             $prevObj = $article->prev();
             $prevLink = get_object_vars($prevObj);
             $prevID = $prevLink['_']['uid']; ?>

             <div class="project-prev paginate">
               <a href="#<?php echo $prevID; ?>" rel="<?php echo $prevID; ?>"> 
                 <h4>Previous Project:</h4> <h3><?php echo $prevObj->title(); ?></h3>
               </a>
             </div>

         <?php } ?>

     </div>
     <div class="project wrapper">


       <div class="content"> 

       <?php echo kirbytext($article->text()) ?>
       <a class="live link" href="<?php echo $article->link() ?>" target="_blank"><?php echo $article->linktext() ?></a>
       <div class="role devel"><span>Role:</span> <?php echo $article->role(); ?></div>
       <div class="credit"><?php echo $article->credit() ?></div>
       
       </div>

       <ul class="slides images">
       <?php foreach($article->images() as $image): ?>
        <?php if($image->hasNext()){   $image = $image->next();  ?>

          <li>
           <img src="<?php echo $image->url() ?>"  alt="<?php echo $image->name() ?>" />
         </li>

        <?php } ?>

       <?php endforeach; ?>
   </section>