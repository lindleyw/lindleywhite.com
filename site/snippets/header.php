<!DOCTYPE html>
<html lang="en">
<head>

  <title><?php echo html($site->title()) ?> - <?php echo html($page->title()) ?></title>
  <meta charset="utf-8" />
  <meta name="description" content="<?php echo html($site->description()) ?>" />
  <meta name="keywords" content="<?php echo html($site->keywords()) ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="index, follow" />
  <meta name="google-site-verification" content="maOASFXnAmCmLCV-LNum8ZlZ4IpfFpDiUtwsi-eW430" />
  <link rel="shortcut icon" href="assets/images/favicon.ico"/>

  <?php echo css('assets/styles/styles.css') ?>
  <link type="text/css" rel="stylesheet" href="https://fast.fonts.net/cssapi/49e8d42b-5a14-412b-9b96-c6512a6f32df.css"/>
  <?php echo js('assets/js/jquery.min.js') ?>

  <link href='https://fonts.googleapis.com/css?family=Francois+One' rel='stylesheet' type='text/css'>



</head>
<?php
		if ($page->parent()) {
			$class = $page->uid() . $page->parent->uid();
		} else {
			$class = $page->uid();
		}
?>

<body class="<?php echo $class ?>">

