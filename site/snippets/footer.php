  <footer>
    <?php echo kirbytext($site->copyright()) ?>
  </footer>
  <?php echo js('assets/js/plugins.js') ?>
 <?php echo js('assets/js/main.js') ?>

 <script>
 $(window).ready(function() {
 	$('.load').click(function(e) {
		e.preventDefault();
		
		$('.projectHolder').load('/ajax/',{id:post_id}).fadeIn();
	})
 });
 </script>
 <?php snippet('ga-file'); ?>
</body>

</html>