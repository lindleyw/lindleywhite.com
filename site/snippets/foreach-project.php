<?php //define variables because PHP hates me


?>

<?php foreach($pages->findByURI('projects')->children()->visible() as $p){


      $imageObj = $p->images()->first();
      if($imageObj != NULL){
        $image_url = $imageObj->url();
      }else {
        $image_url = '';
      }

      $uid = $p->uid();
      $title = $p->title();

  ?>
          <li>
            <a href="#<?php echo $uid ?>" rel="<?php echo $uid ?>">
                <div class="image-wrapper">
                  <img src="<?php echo $image_url ?>"  alt="" />
                  <div class="project-header">
                    <div class="wrap">
                      <h2 class="project-title"><?php echo $p->title(); ?></h2>
                    </div>
                  </div>
                </div>

            </a>
          </li>
<?php } ?>