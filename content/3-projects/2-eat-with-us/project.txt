Template: projects
----
Title: Eat With Us
----
Text: Eat With Us is a brand that holds the franchises for the restaurants The Grill, Harveys, and Sweet Peppers Deli. They wanted a site that a single user could go in and update all three sites with one login, but the sites needed to be different looking and have similar data structures so that they could work with similar inputs. 

Working with a Wordpress multisite as the base we developed unique inputs for the locations and menu along with seperate stylesheets for each site. The sites are designed to be fully responsive and have custom front pages for mobile.  
----
Credit: Agency: <strong>Mabus &amp; SimpleFocus</strong>, Designer: <strong>Alex Jolly</strong>
----
Link: http://eatwithus.com
----
Linktext: Visit Live Site
----
Role: Front-End Development, Web Design
