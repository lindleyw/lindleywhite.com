FROM richarvey/nginx-php-fpm:latest

VOLUME .:/var/www/html

ADD conf/nginx-site.conf /etc/nginx/sites-available/default.conf

# RUN echo http://dl-4.alpinelinux.org/alpine/3.5/community >> /etc/apk/repositories
# RUN apk add php7-xdebug
# RUN echo "zend_extension=/usr/lib/php7/modules/xdebug.so \
# xdebug.coverage_enable=0 \
# xdebug.remote_enable=1 \
# xdebug.remote_connect_back=1 \
# xdebug.remote_log=/tmp/xdebug.log \
# xdebug.remote_autostart=true" | tee -a /etc/php7/php.ini

EXPOSE 80